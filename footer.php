<?php

/**
 * The template for displaying the footer
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */
?>

</main>

<footer id="footer" class="footer">
	<div class="container">
		<div class="footer__column footer__logo">
			<?php
			if (has_custom_logo()) {
				the_custom_logo();
			} else {
				echo '<span class="site-title" style="margin: 0;">' . get_bloginfo('title') . '</span>';
			}
			?>
		</div>
		<!-- /.footer__logo -->

		<?php
		$footer_form = get_theme_mod('microplant_setting_footer_newsletter_form');
		if ($footer_form) :
		?>
			<div class="footer__column footer__form_call">
				<span class="footer__form_title"><?php echo esc_html(get_theme_mod('microplant_setting_footer_newsletter_title', 'Sign up for our newsletter')); ?></span>
				<span class="footer__form_text"><?php echo esc_html(get_theme_mod('microplant_setting_footer_newsletter_text', "Subscribe now so you don't miss any important content!")); ?></span>
			</div>

			<div class="footer__column footer__form">
				<?php echo do_shortcode(get_theme_mod('microplant_setting_footer_newsletter_form')); ?>
			</div>
			<!-- /.footer__form -->
		<?php endif; ?>

		<div class="footer__column footer__about">
			<span class="title"><?php echo sprintf('%s %s', esc_html__('About', 'microplant'), get_bloginfo('title')); ?></span>

			<?php
			if (has_nav_menu('footer_menu')) {
				wp_nav_menu(array(
					'theme_location'  => 'footer_menu',
					'depth'           => 1,
					'container_class' => 'footer__menu_wrap',
					'menu_class'      => 'footer__menu',
				));
			}

			get_template_part('partials/footer/footer', 'social-networks');
			?>
		</div>
		<!-- /.footer__about -->

		<div class="footer__column footer__contacts">
			<span class="title"><?php esc_html_e('Contact', 'microplant'); ?></span>

			<?php get_template_part('partials/footer/footer', 'emails'); ?>
		</div>
		<!-- /.footer__contacts -->

		<div class="footer__column footer__addresses">
			<span class="title"><?php esc_html_e('Where are we', 'microplant'); ?></span>

			<?php get_template_part('partials/footer/footer', 'addresses'); ?>
		</div>
		<!-- /.footer__addresses -->

		<div class="footer__column footer__copyright">
			<ul>
				<li>
					<span>&copy;&nbsp;<?php echo wp_date('Y'); ?>&nbsp;<?php echo get_bloginfo('title'); ?>&nbsp;&#x2010;&nbsp;<?php esc_html_e('All rights reserved.', 'microplant') ?></span>
				</li>
				<li>
					<span><?php echo sprintf("%s %s.", esc_html__('Developed by:', 'microplant'), '<a href="https://www.agenciabeb.com.br/" target="_blank">Agência B&amp;B</a>'); ?></span>
				</li>
			</ul>
		</div>
		<!-- /.footer__copyright -->
	</div>
	<!-- /.container -->
</footer>

<?php wp_footer(); ?>

</body>

</html>