# Micro Plant

<p align="center">
    <img alt="screenshot" title="Screenshot" src="./screenshot.png" />
</p>

This is a WordPress Custom theme made for Micro Plant

## Getting started

Download the code from this repository and place it in a folder inside your WordPress installation themes folder, like this path:

```
\wp-content\themes\microplant

```

Then you will see and be able to activate the theme on your WordPress dashboard > Appearance > Themes

## 🛠 Technologies
This project was developed with the following technologies

- [WordPress](https://br.wordpress.org/)
- [SASS](https://sass-lang.com/)
- [Node.js](https://nodejs.org/)
- [Gulp](https://gulpjs.com/)

