<?php

/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */

get_header();
?>

<?php if (!is_paged()) : ?>
	<?php get_template_part('partials/content/content', 'sticky'); ?>

	<div class="entry-content">
		<?php
		$page_for_posts_id = get_option('page_for_posts');
		$page_for_posts_obj = get_post($page_for_posts_id);

		echo apply_filters('the_content', $page_for_posts_obj->post_content);
		?>
	</div>
	<!-- /.entry-content -->
<?php endif; ?>

<div class="container">
	<?php
	if (have_posts()) {
		while (have_posts()) {
			the_post();
			get_template_part('partials/content/content', 'excerpt');
		}
	} else {
		get_template_part('partials/content/content', 'none');
	}
	?>

	<?php
	the_posts_pagination(array(
		'prev_text' => sprintf(
			'<span aria-label="%s" title="%s">%s</span>',
			esc_html__('Previous', 'microplant'),
			esc_html__('Previous', 'microplant'),
			file_get_contents(get_template_directory_uri() . '/assets/svg/chevron-left.svg')
		),
		'next_text' => sprintf(
			'<span aria-label="%s" title="%s">%s</span>',
			esc_html__('Next', 'microplant'),
			esc_html__('Next', 'microplant'),
			file_get_contents(get_template_directory_uri() . '/assets/svg/chevron-right.svg')
		),
	));
	?>
</div>
<!-- /.container -->

<?php

get_footer();
