const path = require( 'path' );
const webpack = require( 'webpack' );

module.exports = {
	entry: {
		frontend: './assets/js/frontend/frontend.js',
	},
	output: {
		filename: '[name].min.js',
		path: path.resolve( __dirname, 'assets', 'js', 'frontend' ),
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: [ '@babel/preset-env' ],
					},
				},
			},
			{
				test: /\.svg$/,
				loader: 'svg-inline-loader',
			},
		],
	},
};
