<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php wp_body_open(); ?>

	<header id="header" class="header">
		<?php get_template_part('partials/header/header', 'navbar') ?>

		<?php if (!is_page() && !is_404()) : ?>
			<div class="header__inner">
				<?php
				if (is_single() && has_post_thumbnail()) {
					$header_image = esc_url(get_the_post_thumbnail_url(get_the_ID(), 'full'));
				} else {
					$header_image = esc_url(get_header_image());
				}
				?>

				<div class="header__image" style="background-image: url( <?php echo ($header_image); ?> );">
				</div>
				<!-- /.header__image -->

				<div class="header__meta">
					<div class="container">
						<?php
						if (is_home() && !empty(STICKY_CATEGORY_ID) && !is_paged()) {

							$stickyPosts = new WP_Query(array(
								'post_type'      => 'post',
								'cat'            => (int)STICKY_CATEGORY_ID,
								'posts_per_page' => 2,
								'post__in'       =>	get_option('sticky_posts'),
							));

							if ($stickyPosts->have_posts()) {
								echo sprintf('<span class="title">%s</span>', get_cat_name((int)STICKY_CATEGORY_ID));
								echo sprintf('<div class="taxonomy-description">%s</div>', category_description((int)STICKY_CATEGORY_ID));
								wp_reset_query();
							}
						} else if (is_home()) echo sprintf('<h1 class="title">%s</h1>', get_the_title(get_option('page_for_posts', true)));

						if (is_archive()) {
							echo sprintf('<h1 class="title">%s</h1>', single_term_title("", false));
							the_archive_description('<div class="taxonomy-description">', '</div>');
						}

						if (is_author()) echo sprintf('<h1 class="title">%s %s</h1>', esc_html__("Author:", 'micropalt'), get_the_author_meta('display_name'));
						if (is_single()) echo sprintf('<h1 class="title">%s</h1>', get_the_title());
						?>
					</div>
					<!-- /.container -->
				</div>
				<!-- /.header__meta -->
			</div>
			<!-- /.header__inner -->
		<?php endif; ?>

	</header>

	<main>