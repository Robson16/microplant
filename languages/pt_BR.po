msgid ""
msgstr ""
"Project-Id-Version: Micro Plant\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-10-26 18:07+0000\n"
"PO-Revision-Date: 2023-11-23 18:59+0000\n"
"Last-Translator: Agência B&amp;B\n"
"Language-Team: Português do Brasil\n"
"Language: pt_BR\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/\n"
"X-Loco-Version: 2.6.6; wp-6.4.1\n"
"X-Domain: microplant"

#: footer.php:41
msgid "About"
msgstr "Sobre"

#: includes/kirki/kirki-control-addresses.php:42
#: includes/kirki/kirki-control-addresses.php:115
msgid "Add another address"
msgstr "Adicione outro endereço"

#: includes/kirki/kirki-control-contacts.php:44
#: includes/kirki/kirki-control-contacts.php:113
msgid "Add another email"
msgstr "Adicionar outro e-mail"

#: includes/kirki/kirki-control-addresses.php:39
#: includes/kirki/kirki-control-addresses.php:112
msgid "Address"
msgstr "Endereço"

#: includes/kirki/kirki-control-addresses.php:62
#: includes/kirki/kirki-control-addresses.php:135
msgid "Address Text"
msgstr "Texto do endereço"

#: includes/kirki/kirki-control-addresses.php:57
#: includes/kirki/kirki-control-addresses.php:130
msgid "Address Title"
msgstr "Título do endereço"

#: includes/kirki/kirki-control-addresses.php:67
#: includes/kirki/kirki-control-addresses.php:140
msgid "Address URL"
msgstr "URL do endereço"

#: includes/kirki/kirki-control-addresses.php:15
#: includes/kirki/kirki-control-addresses.php:34
#: includes/kirki/kirki-control-addresses.php:98
#: includes/kirki/kirki-control-addresses.php:107
msgid "Addresses"
msgstr "Endereços"

#: includes/kirki/kirki-control-addresses.php:25
#, php-format
msgid "Addresses ( %s )"
msgstr "Endereços ( %s )"

#: includes/kirki/kirki-control-addresses.php:16
#: includes/kirki/kirki-control-addresses.php:99
msgid "Addresses to use on the website."
msgstr "Endereços para usar no site."

#. 1: dashboard link.
#: includes/required-plugins.php:175
#, php-format
msgid "All plugins installed and activated successfully. %1$s"
msgstr "Todos os plugins instalados e ativados com sucesso. %1$s"

#: footer.php:75
msgid "All rights reserved."
msgstr "Todos os direitos reservados."

#: includes/required-plugins.php:162
msgid "Begin activating plugin"
msgid_plural "Begin activating plugins"
msgstr[0] "Comece a ativar o plugin"
msgstr[1] "Comece a ativar plug-ins"

#: includes/required-plugins.php:152
msgid "Begin installing plugin"
msgid_plural "Begin installing plugins"
msgstr[0] "Comece a instalar o plugin"
msgstr[1] "Comece a instalar plug-ins"

#: includes/required-plugins.php:157
msgid "Begin updating plugin"
msgid_plural "Begin updating plugins"
msgstr[0] "Comece a atualizar o plugin"
msgstr[1] "Comece a atualizar plug-ins"

#: includes/kirki/kirki-control-blog.php:11
msgid "Blog"
msgstr "Blogue"

#: partials/content/content.php:23
msgid "Categories:"
msgstr "Categorias:"

#: includes/kirki/kirki-control-blog.php:33
msgid "Choose a category to be highlighted on the blog's main page."
msgstr "Escolha uma categoria para ser destacada na página principal do blog."

#: includes/kirki/kirki-control-blog.php:35
msgid "Choose an category"
msgstr "Escolha uma categoria"

#: comments.php:23
msgid "Comments"
msgstr "Comentários"

#: comments.php:46
msgid "Comments closed."
msgstr "Comentários encerrados."

#: footer.php:59
msgid "Contact"
msgstr "Contato"

#: includes/kirki/kirki-control-contacts.php:17
#: includes/kirki/kirki-control-contacts.php:87
msgid "Contact Infos"
msgstr "Informações de contato"

#: includes/kirki/kirki-control-contacts.php:18
#: includes/kirki/kirki-control-contacts.php:88
msgid "Contact Infos to use around the site."
msgstr "Informações de contato para usar em todo o site."

#: footer.php:78
msgid "Developed by:"
msgstr "Desenvolvido por:"

#: includes/required-plugins.php:176
msgid "Dismiss this notice"
msgstr "Descartar essa notificação"

#: includes/kirki/kirki-control-contacts.php:96
msgid "Email"
msgstr "E-mail"

#: includes/kirki/kirki-control-contacts.php:27
#, php-format
msgid "Email ( %s )"
msgstr "E-mail (%s)"

#: includes/kirki/kirki-control-contacts.php:65
#: includes/kirki/kirki-control-contacts.php:134
msgid "Email Address"
msgstr "Endereço de email"

#: includes/kirki/kirki-control-contacts.php:36
#: includes/kirki/kirki-control-contacts.php:105
msgid "Email list"
msgstr "Lista de e-mail"

#: includes/kirki/kirki-control-contacts.php:60
#: includes/kirki/kirki-control-contacts.php:129
msgid "Email Title"
msgstr "Título do e-mail"

#: includes/kirki/kirki-control-contacts.php:41
#: includes/kirki/kirki-control-contacts.php:110
msgid "Emails"
msgstr "E-mails"

#: includes/kirki/kirki-control-blog.php:12
msgid "Extra options to customize the blog."
msgstr "Opções extras para personalizar o blog."

#: includes/kirki/kirki-control-footer.php:12
msgid "Extra options to customize the footer."
msgstr "Opções extras para personalizar o rodapé."

#: includes/kirki/kirki-control-social-networks.php:30
msgid "Facebook"
msgstr "Facebook"

#: includes/kirki/kirki-control-footer.php:11
msgid "Footer"
msgstr "Rodapé"

#: functions.php:53
msgid "Footer Menu"
msgstr "Menu de Rodapé"

#. Description of the theme
msgid "Full-Site Editing WordPress Theme for Micro Plant"
msgstr "Tema WordPress de edição completa do site para Micro Plant"

#. URI of the theme
msgid "https://gitlab.com/Robson16/microplant.git"
msgstr "https://gitlab.com/Robson16/microplant.git"

#. Author URI of the theme
msgid "https://www.robsonhrodrigues.com.br/"
msgstr "https://www.robsonhrodrigues.com.br/"

#: includes/kirki/kirki-control-social-networks.php:40
msgid "Instagram"
msgstr "Instagram"

#: includes/required-plugins.php:110
msgid "Install Plugins"
msgstr "Instalar plug-ins"

#: includes/required-plugins.php:109
msgid "Install Required Plugins"
msgstr "Instale os plug-ins necessários"

#. %s: plugin name.
#: includes/required-plugins.php:112
#, php-format
msgid "Installing Plugin: %s"
msgstr "Instalando o plug-in: %s"

#: comments.php:52
msgid "Leave a comment."
msgstr "Deixe um comentário."

#: functions.php:52
msgid "Main Menu"
msgstr "Menu principal"

#. Name of the theme
msgid "Micro Plant"
msgstr "Micro Plant"

#: includes/kirki/kirki-control-contacts.php:73
#: includes/kirki/kirki-control-contacts.php:142
msgid "New Window"
msgstr "Nova janela"

#: includes/kirki/kirki-control-footer.php:39
msgid "Newsletter Form Shortcode"
msgstr "Código abreviado do formulário de boletim informativo"

#: includes/kirki/kirki-control-footer.php:30
msgid "Newsletter Text"
msgstr "Texto do boletim informativo"

#: includes/kirki/kirki-control-footer.php:20
msgid "Newsletter Title"
msgstr "Título do boletim informativo"

#: home.php:54 home.php:55 index.php:40 index.php:41
msgid "Next"
msgstr "Próximo"

#. 1: plugin name.
#: includes/required-plugins.php:171
#, php-format
msgid "No action taken. Plugin %1$s was already active."
msgstr "Nenhuma ação tomada. O plug-in %1$s já estava ativo."

#: partials/content/content-none.php:8
msgid "No content to display"
msgstr "Nenhum conteúdo para exibir"

#: includes/kirki/kirki-control-addresses.php:72
#: includes/kirki/kirki-control-addresses.php:145
msgid "Phone 1"
msgstr "Telefone 1"

#: includes/kirki/kirki-control-addresses.php:77
#: includes/kirki/kirki-control-addresses.php:150
msgid "Phone 2"
msgstr "Telefone 2"

#: includes/required-plugins.php:178
msgid "Please contact the administrator of this site for help."
msgstr "Entre em contato com o administrador deste site para obter ajuda."

#: includes/required-plugins.php:168
msgid "Plugin activated successfully."
msgstr "Plugin ativado com sucesso."

#. 1: plugin name.
#: includes/required-plugins.php:173
#, php-format
msgid ""
"Plugin not activated. A higher version of %s is needed for this theme. "
"Please update the plugin."
msgstr ""
"Plug-in não ativado. Uma versão superior de %s é necessária para este tema. "
"Atualize o plug-in."

#: partials/content/content.php:20
msgid "Posted in"
msgstr "Postado em"

#: home.php:48 home.php:49 index.php:34 index.php:35
msgid "Previous"
msgstr "Anterior"

#: partials/content/content-excerpt.php:32
msgid "Read more"
msgstr "Leia mais"

#: 404.php:16
msgid "Return to home page"
msgstr "Retornar a página inicial"

#: includes/required-plugins.php:167
msgid "Return to Required Plugins Installer"
msgstr "Voltar para instalador de Plugins necessários"

#. Author of the theme
msgid "Robson H Rodrigues"
msgstr "Robson H Rodrigues"

#: includes/kirki/kirki-control-contacts.php:74
#: includes/kirki/kirki-control-contacts.php:143
msgid "Same Frame"
msgstr "Mesmo quadro"

#: includes/kirki/kirki-control-footer.php:22
msgid "Sign up for our newsletter"
msgstr "Assine a nossa newsletter"

#: includes/kirki/kirki-control-social-networks.php:11
msgid "Social Networks"
msgstr "Redes sociais"

#: includes/kirki/kirki-control-social-networks.php:12
msgid "Social networks to use around the site."
msgstr "Redes sociais para usar no site."

#: includes/required-plugins.php:115
msgid "Something went wrong with the plugin API."
msgstr "Algo deu errado com a API do plugin."

#: includes/kirki/kirki-control-blog.php:32
msgid "Sticky Category"
msgstr "Categoria fixa"

#: includes/kirki/kirki-control-footer.php:32
msgid "Subscribe now so you don't miss any important content!"
msgstr "Inscreva-se agora para não perder nenhum conteúdo importante!"

#: partials/content/content.php:27
msgid "Tags:"
msgstr "Tag:"

#: includes/kirki/kirki-control-contacts.php:70
#: includes/kirki/kirki-control-contacts.php:139
msgid "Target"
msgstr "Alvo"

#: includes/required-plugins.php:128
#, php-format
msgid ""
"The following plugin needs to be updated to its latest version to ensure "
"maximum compatibility with this theme: %1$s."
msgid_plural ""
"The following plugins need to be updated to their latest version to ensure "
"maximum compatibility with this theme: %1$s."
msgstr[0] ""
"O seguinte plugin precisa ser atualizado para sua versão mais recente para "
"garantir compatibilidade máxima com este tema: %1$s."
msgstr[1] ""
"Os seguintes plugins precisam ser atualizados para a versão mais recente "
"para garantir a máxima compatibilidade com este tema: %1$s."

#: includes/required-plugins.php:169
msgid "The following plugin was activated successfully:"
msgstr "O seguinte plugin foi ativado com sucesso:"

#: includes/required-plugins.php:146
#, php-format
msgid "The following recommended plugin is currently inactive: %1$s."
msgid_plural "The following recommended plugins are currently inactive: %1$s."
msgstr[0] "O seguinte plugin recomendado está atualmente inativo: %1$s."
msgstr[1] "Os seguintes plugins recomendados estão atualmente inativos: %1$s."

#: includes/required-plugins.php:140
#, php-format
msgid "The following required plugin is currently inactive: %1$s."
msgid_plural "The following required plugins are currently inactive: %1$s."
msgstr[0] "O seguinte plugin necessário está atualmente inativo: %1$s."
msgstr[1] "Os seguintes plugins necessários estão atualmente inativos: %1$s."

#: 404.php:15
msgid "The page you are looking for does not exist."
msgstr "A página que você procura não existe."

#: includes/required-plugins.php:177
msgid ""
"There are one or more required or recommended plugins to install, update or "
"activate."
msgstr ""
"Existem um ou mais plug-ins obrigatórios ou recomendados para instalar, "
"atualizar ou ativar."

#: includes/required-plugins.php:134
#, php-format
msgid "There is an update available for: %1$s."
msgid_plural "There are updates available for the following plugins: %1$s."
msgstr[0] "Há uma atualização disponível para: %1$s."
msgstr[1] "Existem atualizações disponíveis para os seguintes plugins: %1$s."

#: includes/required-plugins.php:122
#, php-format
msgid "This theme recommends the following plugin: %1$s."
msgid_plural "This theme recommends the following plugins: %1$s."
msgstr[0] "Este tema recomenda o seguinte plugin: %1$s."
msgstr[1] "Este tema recomenda os seguintes plugins: %1$s."

#: includes/required-plugins.php:116
#, php-format
msgid "This theme requires the following plugin: %1$s."
msgid_plural "This theme requires the following plugins: %1$s."
msgstr[0] "Este tema requer o seguinte plugin: %1$s."
msgstr[1] "Este tema requer os seguintes plugins: %1$s."

#. %s: plugin name.
#: includes/required-plugins.php:114
#, php-format
msgid "Updating Plugin: %s"
msgstr "Atualizando plug-in: %s"

#: includes/kirki/kirki-control-social-networks.php:50
msgid "Whatsapp"
msgstr "Whatsapp"

#: includes/kirki/kirki-control-addresses.php:82
#: includes/kirki/kirki-control-addresses.php:155
msgid "Whatsapp 1"
msgstr "Whatsapp 1"

#: includes/kirki/kirki-control-addresses.php:87
#: includes/kirki/kirki-control-addresses.php:160
msgid "Whatsapp 2"
msgstr "Whatsapp 2"

#: footer.php:66
msgid "Where are we"
msgstr "Onde estamos"

#: includes/kirki/kirki-control-social-networks.php:20
msgid "You Tube"
msgstr "YouTube"
