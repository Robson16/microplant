<?php

/**
 * Header Main Navbar
 *
 */

?>

<nav id="navbar" class="navbar">
	<div class="container">
		<span class="navbar-brand">
			<?php
			if (has_custom_logo()) {
				the_custom_logo();
			} else {
				echo '<h1 class="site-title" style="margin: 0;">' . get_bloginfo('title') . '</h1>';
			}
			?>
		</span>

		<?php if (has_nav_menu('main_menu')) : ?>
			<button type="button" class="navbar-toggler" data-target="#navbar-nav">
				<span class="navbar-toggler-icon">
					<div class="bar1"></div>
					<div class="bar2"></div>
					<div class="bar3"></div>
				</span>
			</button>

			<?php
			wp_nav_menu(array(
				'theme_location'      => 'main_menu',
				'depth'               => 2,
				'container'           => 'div',
				'container_class'     => 'collapse navbar-collapse',
				'container_id'        => 'navbar-nav',
				'menu_class'          => 'navbar-nav',
				'fallback_cb'         => 'WP_Bootstrap_Navwalker::fallback',
				'walker'              => new WP_Bootstrap_Navwalker()
			));
			?>
		<?php endif; ?>
	</div>
	<!-- /.container -->
</nav>
<!-- /.navbar -->