<?php

/**
 * Template part for Footer Email list
 *
 */

// Default values for 'microplant_setting_contact_emails' theme mod.
$defaults = array(
	array(
		'email_title'       => 'Comercial',
		'email_address'     => 'comercial@microplant.com.br',
		'email_target'      => '_blank',
	),
	array(
		'email_title'       => 'SAC',
		'email_address'     => 'sac@microplant.com.br',
		'email_target'      => '_blank',
	),
);
?>

<?php if (function_exists('pll_count_posts')) : ?>
	<?php $translations = pll_the_languages(array('raw' => 1)); ?>

	<?php foreach ($translations as $language) : ?>
		<?php if ($language['current_lang']) : ?>

			<?php $email_list = get_theme_mod('microplant_setting_contact_emails_' . $language['slug'], $defaults); ?>

			<?php if ($email_list) : ?>
				<span class="icon-text">
					<i>
						<?php echo file_get_contents(get_template_directory_uri() . '/assets/svg/envelope.svg'); ?>
					</i>
					<ul>
						<?php foreach ($email_list as $email_item) : ?>
							<li>
								<a href="<?php echo 'mailto:' . esc_html($email_item['email_address']); ?>" target="<?php echo esc_attr($email_item['email_target']); ?>">
									<?php echo esc_html($email_item['email_address']); ?>
								</a>
							</li>
						<?php endforeach; ?>
					</ul>
				</span>
			<?php endif; ?>

			<?php return; ?>

		<?php endif; ?>
	<?php endforeach; ?>

<?php else : ?>

	<?php $email_list = get_theme_mod('microplant_setting_contact_emails', $defaults); ?>

	<?php if ($email_list) : ?>
		<span class="icon-text">
			<i>
				<?php echo file_get_contents(get_template_directory_uri() . '/assets/svg/envelope.svg'); ?>
			</i>
			<ul>
				<?php foreach ($email_list as $email_item) : ?>
					<li>
						<a href="<?php echo 'mailto:' . esc_html($email_item['email_address']); ?>" target="<?php echo esc_attr($email_item['email_target']); ?>">
							<?php echo esc_html($email_item['email_address']); ?>
						</a>
					</li>
				<?php endforeach; ?>
			</ul>
		</span>
	<?php endif; ?>

<?php endif; ?>