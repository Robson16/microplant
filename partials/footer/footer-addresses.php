<?php

/**
 * Template part for Footer Addresses
 *
 */

// Default values for 'microplant_setting_addresses' theme mod.
$defaults = array(
	array(
		'address_title'         => 'São Paulo',
		'address_text'          => 'Av Leôncio de Magalhães, 1509 Jardim São Paulo – SP',
		'address_url'           => 'https://goo.gl/maps/xvKA1tegbBH1EKPG9',
		'address_phone_1'       => '(11) 4444-5555',
		'address_phone_2'       => '(11) 4444-5555',
		'address_whatsapp_1'    => '(11) 97777-8888',
		'address_whatsapp_2'    => '(11) 97777-8888',
	),
);
?>

<?php if (function_exists('pll_count_posts')) : ?>

	<?php $translations = pll_the_languages(array('raw' => 1)); ?>

	<?php foreach ($translations as $language) : ?>
		<?php if ($language['current_lang']) : ?>

			<?php $addresses = get_theme_mod('microplant_setting_addresses_' . $language['slug'], $defaults); ?>

			<?php if ($addresses) :	?>
				<?php foreach ($addresses as $address) : ?>
					<div>
						<span class="icon-text">
							<i>
								<?php echo file_get_contents(get_template_directory_uri() . '/assets/svg/marker.svg'); ?>
							</i>
							<ul>
								<li>
									<span class="footer__addresses_title"><?php echo esc_html($address['address_title']); ?></span>
								</li>
								<li>
									<a href="<?php echo esc_url($address['address_url']); ?>" target="_blank">
										<?php echo esc_html($address['address_text']); ?>
									</a>
								</li>
							</ul>
						</span>
					</div>
				<?php endforeach; ?>
			<?php endif; ?>

			<?php return; ?>

		<?php endif; ?>
	<?php endforeach; ?>

<?php else : ?>

	<?php $addresses = get_theme_mod('microplant_setting_addresses', $defaults); ?>

	<?php if ($addresses) :	?>
		<?php foreach ($addresses as $address) : ?>
			<div class="footer__addresses_address">
				<span class="icon-text">
					<i>
						<?php echo file_get_contents(get_template_directory_uri() . '/assets/svg/marker.svg'); ?>
					</i>
					<ul>
						<li>
							<span class="footer__addresses_title"><?php echo esc_html($address['address_title']); ?></span>
						</li>
						<li>
							<a href="<?php echo esc_url($address['address_url']); ?>" target="_blank">
								<?php echo esc_html($address['address_text']); ?>
							</a>
						</li>
					</ul>
				</span>
				<?php if ($address['address_phone_1'] || $address['address_phone_2']) : ?>
					<span class="icon-text">
						<i>
							<?php echo file_get_contents(get_template_directory_uri() . '/assets/svg/call-incoming.svg'); ?>
						</i>
						<span>
							<?php if ($address['address_phone_1']) : ?>
								<a href="#">
									<?php echo esc_html($address['address_phone_1']); ?>
								</a>
							<?php endif; ?>
							<?php if ($address['address_phone_1'] && $address['address_phone_2']) : ?>
								&#124;
							<?php endif; ?>
							<?php if ($address['address_phone_2']) : ?>
								<a href="#">
									<?php echo esc_html($address['address_phone_2']); ?>
								</a>
							<?php endif; ?>
						</span>
					</span>
				<?php endif; ?>
				<?php if ($address['address_whatsapp_1'] || $address['address_whatsapp_2']) : ?>
					<span class="icon-text">
						<i>
							<?php echo file_get_contents(get_template_directory_uri() . '/assets/svg/whatsapp.svg'); ?>
						</i>
						<span>
							<?php if ($address['address_whatsapp_1']) : ?>
								<a href="<?php echo 'https://api.whatsapp.com/send?phone=55' . preg_replace("/[^0-9]/", "", esc_html($address['address_whatsapp_1'])); ?>">
									<?php echo esc_html($address['address_whatsapp_1']); ?>
								</a>
							<?php endif; ?>
							<?php if ($address['address_whatsapp_1'] && $address['address_whatsapp_2']) : ?>
								&#124;
							<?php endif; ?>
							<?php if ($address['address_whatsapp_2']) : ?>
								<a href="<?php echo 'https://api.whatsapp.com/send?phone=55' . preg_replace("/[^0-9]/", "", esc_html($address['address_whatsapp_2'])); ?>">
									<?php echo esc_html($address['address_whatsapp_2']); ?>
								</a>
							<?php endif; ?>
						</span>
					</span>
				<?php endif; ?>
			</div>
		<?php endforeach; ?>
	<?php endif; ?>

<?php endif; ?>