<?php

/**
 * Template part for Footer Social Networks
 *
 */

?>


<ul class="social-networks">
	<?php
	$social_youtube = get_theme_mod('microplant_setting_social_youtube', 'https://www.youtube.com/');
	if ($social_youtube) :
	?>
		<li>
			<a href="<?php echo esc_url($social_youtube); ?>" target="_blank" aria-label="You Tube">
				<i class="icon">
					<?php echo file_get_contents(get_template_directory_uri() . '/assets/svg/youtube.svg'); ?>
				</i>
			</a>
		</li>
	<?php endif; ?>

	<?php
	$social_facebook = get_theme_mod('microplant_setting_social_facebook', 'https://www.facebook.com/');
	if ($social_facebook) :
	?>
		<li>
			<a href="<?php echo esc_url($social_facebook); ?>" target="_blank" aria-label="Facebook">
				<i class="icon">
					<?php echo file_get_contents(get_template_directory_uri() . '/assets/svg/facebook.svg'); ?>
				</i>
			</a>
		</li>
	<?php endif; ?>

	<?php
	$social_instagram = get_theme_mod('microplant_setting_social_instagram', 'https://www.instagram.com/');
	if ($social_instagram) :
	?>
		<li>
			<a href="<?php echo esc_url($social_instagram); ?>" target="_blank" aria-label="Instagram">
				<i class="icon">
					<?php echo file_get_contents(get_template_directory_uri() . '/assets/svg/instagram.svg'); ?>
				</i>
			</a>
		</li>
	<?php endif; ?>

	<?php
	$social_whatsapp = get_theme_mod('microplant_setting_social_whatsapp', 'https://api.whatsapp.com/send?phone=5511988887777&text=Ol%C3%A1');
	if ($social_whatsapp) :
	?>
		<li>
			<a href="<?php echo esc_url($social_whatsapp); ?>" target="_blank" aria-label="Whatsapp">
				<i class="icon">
					<?php echo file_get_contents(get_template_directory_uri() . '/assets/svg/whatsapp.svg'); ?>
				</i>
			</a>
		</li>
	<?php endif; ?>

</ul>