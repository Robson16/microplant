<?php

/**
 * Template part for displaying post archives and search results
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 */

?>

<div id="post-<?php the_ID(); ?>" <?php post_class('post-excerpt'); ?>>
	<div>
		<a class="post-excerpt__thumbnail" href="<?php echo esc_url(get_permalink()); ?>">
			<?php if (has_post_thumbnail()) : ?>
				<?php the_post_thumbnail('medium_large', array('title' => get_the_title())); ?>
			<?php else : ?>
				<img width="758" height="512" src="<?php echo get_template_directory_uri() . '/assets/images/placeholder.jpg'; ?>" alt="placeholder">
			<?php endif; ?>
		</a>
	</div>

	<div>
		<span>
			<?php the_title(sprintf('<h3 class="post-excerpt__title"><a href="%s" rel="bookmark">', esc_url(get_permalink())), '</a></h3>');		?>

			<span class="post-excerpt__excerpt">
				<?php the_excerpt(); ?>
			</span>
		</span>

		<a class="post-excerpt__read-more" href="<?php echo esc_url(get_permalink()); ?>">
			<?php _e('Read more', 'microplant') ?>
			<i class="icon-arrow-right-up"></i>
		</a>
	</div>

</div><!-- #post-<?php the_ID(); ?> -->