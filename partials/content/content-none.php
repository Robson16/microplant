<?php
/*
 * Template Part to display that no posts were found
 */
?>

<div>
	<h2 class="text-center"><?php esc_html_e('No content to display', 'microplant'); ?></h2>
</div>