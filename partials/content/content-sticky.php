<?php

/**
 * Displaying the sticky posts for a given category
 *
 */

if (!empty(STICKY_CATEGORY_ID)) :
	$stickyPosts = new WP_Query(array(
		'post_type'      => 'post',
		'cat'            => (int)STICKY_CATEGORY_ID,
		'posts_per_page' => 2,
		'post__in'       =>	get_option('sticky_posts'),
	));

	if ($stickyPosts->have_posts()) :
?>

		<div class="container">
			<?php
			while ($stickyPosts->have_posts()) {
				$stickyPosts->the_post();
				get_template_part('partials/content/content', 'excerpt');
			}

			wp_reset_query();
			?>
		</div>
		<!-- /.container -->

<?php
	endif;

endif;
