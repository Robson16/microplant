<?php

/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 */

get_header();

while (have_posts()) {
	the_post();
	get_template_part("partials/content/content", get_post_format());
}
?>

<section class="container comments-section">
	<?php if (comments_open() || get_comments_number()) comments_template(); ?>
</section>

<?php

get_footer();
