<?php

/**
 * Kirki Customizer - List of Addresses
 *
 */

// If Polylang is active
if (function_exists('pll_count_posts')) {
	$translations = pll_the_languages(array('raw' => 1));

	new \Kirki\Panel(
		'microplant_panel_header_addresses',
		array(
			'title'       => esc_html__('Addresses', 'microplant'),
			'description' => esc_html__('Addresses to use on the website.', 'microplant'),
			'priority'    => 160,
		)
	);

	foreach ($translations as $language) {
		new \Kirki\Section(
			'microplant_section_addresses_' . $language['slug'],
			array(
				'title'       => sprintf(esc_html__('Addresses ( %s )', 'microplant'), $language['name']),
				'panel'       => 'microplant_panel_header_addresses',
				'priority'    => 160,
			)
		);

		new \Kirki\Field\Repeater(
			array(
				'settings' => 'microplant_setting_addresses_' . $language['slug'],
				'label'    => esc_html__('Addresses', 'microplant'),
				'section'  => 'microplant_section_addresses_' . $language['slug'],
				'priority' => 10,
				'row_label' => array(
					'type'  => 'field',
					'value' => esc_html__('Address', 'microplant'),
					'field' => 'address_title',
				),
				'button_label' => esc_html__('Add another address', 'microplant'),
				'default'  => array(
					array(
						'address_title'         => 'São Paulo',
						'address_text'          => 'Av Leôncio de Magalhães, 1509 Jardim São Paulo – SP',
						'address_url'           => 'https://goo.gl/maps/xvKA1tegbBH1EKPG9',
						'address_phone_1'       => '(11) 4444-5555',
						'address_phone_2'       => '(11) 4444-5555',
						'address_whatsapp_1'    => '(11) 97777-8888',
						'address_whatsapp_2'    => '(11) 97777-8888',
					),
				),
				'fields'   => array(
					'address_title'   => array(
						'type'        => 'text',
						'label'       => esc_html__('Address Title', 'microplant'),
						'default'     => '',
					),
					'address_text'    => array(
						'type'        => 'text',
						'label'       => esc_html__('Address Text', 'microplant'),
						'default'     => '',
					),
					'address_url'    => array(
						'type'        => 'url',
						'label'       => esc_html__('Address URL', 'microplant'),
						'default'     => '',
					),
					'address_phone_1'    => array(
						'type'        => 'text',
						'label'       => esc_html__('Phone 1', 'microplant'),
						'default'     => '(11) 4444-5555',
					),
					'address_phone_2'    => array(
						'type'        => 'text',
						'label'       => esc_html__('Phone 2', 'microplant'),
						'default'     => '(11) 4444-5555',
					),
					'address_whatsapp_1'    => array(
						'type'        => 'text',
						'label'       => esc_html__('Whatsapp 1', 'microplant'),
						'default'     => '(11) 97777-8888',
					),
					'address_whatsapp_2'    => array(
						'type'        => 'text',
						'label'       => esc_html__('Whatsapp 2', 'microplant'),
						'default'     => '(11) 97777-8888',
					),
				),
			)
		);
	}
} else {
	new \Kirki\Section(
		'microplant_section_addresses',
		array(
			'title'       => esc_html__('Addresses', 'microplant'),
			'description' => esc_html__('Addresses to use on the website.', 'microplant'),
			'priority'    => 160,
		)
	);

	new \Kirki\Field\Repeater(
		array(
			'settings' => 'microplant_setting_addresses',
			'label'    => esc_html__('Addresses', 'microplant'),
			'section'  => 'microplant_section_addresses',
			'priority' => 10,
			'row_label' => array(
				'type'  => 'field',
				'value' => esc_html__('Address', 'microplant'),
				'field' => 'address_title',
			),
			'button_label' => esc_html__('Add another address', 'microplant'),
			'default'  => array(
				array(
					'address_title'         => 'São Paulo',
					'address_text'          => 'Av Leôncio de Magalhães, 1509 Jardim São Paulo – SP',
					'address_url'           => 'https://goo.gl/maps/xvKA1tegbBH1EKPG9',
					'address_phone_1'       => '(11) 4444-5555',
					'address_phone_2'       => '(11) 4444-5555',
					'address_whatsapp_1'    => '(11) 97777-8888',
					'address_whatsapp_2'    => '(11) 97777-8888',
				),
			),
			'fields'   => array(
				'address_title'   => array(
					'type'        => 'text',
					'label'       => esc_html__('Address Title', 'microplant'),
					'default'     => '',
				),
				'address_text'    => array(
					'type'        => 'text',
					'label'       => esc_html__('Address Text', 'microplant'),
					'default'     => '',
				),
				'address_url'    => array(
					'type'        => 'url',
					'label'       => esc_html__('Address URL', 'microplant'),
					'default'     => '',
				),
				'address_phone_1'    => array(
					'type'        => 'text',
					'label'       => esc_html__('Phone 1', 'microplant'),
					'default'     => '(11) 4444-5555',
				),
				'address_phone_2'    => array(
					'type'        => 'text',
					'label'       => esc_html__('Phone 2', 'microplant'),
					'default'     => '(11) 4444-5555',
				),
				'address_whatsapp_1'    => array(
					'type'        => 'text',
					'label'       => esc_html__('Whatsapp 1', 'microplant'),
					'default'     => '(11) 97777-8888',
				),
				'address_whatsapp_2'    => array(
					'type'        => 'text',
					'label'       => esc_html__('Whatsapp 2', 'microplant'),
					'default'     => '(11) 97777-8888',
				),
			),
		)
	);
}
