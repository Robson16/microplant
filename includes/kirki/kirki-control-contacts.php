<?php

/**
 * Kirki Customizer - Contacts Infos
 *
 */

// If Polylang is active
if (function_exists('pll_count_posts')) {
	$translations = pll_the_languages(array('raw' => 1));

	// Panel
	new \Kirki\Panel(
		'microplant_panel_contact_infos',
		array(
			'priority'    => 160,
			'title'       => esc_html__('Contact Infos', 'microplant'),
			'description' => esc_html__('Contact Infos to use around the site.', 'microplant'),
		)
	);

	foreach ($translations as $language) {
		// Section - Emails
		new \Kirki\Section(
			'microplant_section_contact_emails_' . $language['slug'],
			array(
				'title'       => sprintf(esc_html__('Email ( %s )', 'microplant'), $language['name']),
				'panel'       => 'microplant_panel_contact_infos',
				'priority'    => 160,
			)
		);

		new \Kirki\Field\Repeater(
			array(
				'settings' => 'microplant_setting_contact_emails_' . $language['slug'],
				'label'    => esc_html__('Email list', 'microplant'),
				'section'  => 'microplant_section_contact_emails_' . $language['slug'],
				'priority' => 10,
				'row_label' => array(
					'type'  => 'field',
					'value' => esc_html__('Emails', 'microplant'),
					'field' => 'email_title',
				),
				'button_label' => esc_html__('Add another email', 'microplant'),
				'default'  => [
					[
						'email_title'       => 'Comercial',
						'email_address'     => 'comercial@microplant.com.br',
						'email_target'      => '_blank',
					],
					[
						'email_title'       => 'SAC',
						'email_address'     => 'sac@microplant.com.br',
						'email_target'      => '_blank',
					],
				],
				'fields'   => [
					'email_title'   => [
						'type'        => 'text',
						'label'       => esc_html__('Email Title', 'microplant'),
						'default'     => '',
					],
					'email_address'    => [
						'type'        => 'text',
						'label'       => esc_html__('Email Address', 'microplant'),
						'default'     => '',
					],
					'email_target' => [
						'type'        => 'select',
						'label'       => esc_html__('Target', 'microplant'),
						'default'     => '_blank',
						'choices'     => [
							'_blank' => esc_html__('New Window', 'microplant'),
							'_self'  => esc_html__('Same Frame', 'microplant'),
						],
					],
				],
			)
		);
	}
} else {
	// Panel
	new \Kirki\Panel(
		'microplant_panel_contact_infos',
		[
			'priority'    => 160,
			'title'       => esc_html__('Contact Infos', 'microplant'),
			'description' => esc_html__('Contact Infos to use around the site.', 'microplant'),
		]
	);

	// Section - Emails
	new \Kirki\Section(
		'microplant_section_contact_emails',
		array(
			'title'       => esc_html__('Email', 'microplant'),
			'panel'       => 'microplant_panel_contact_infos',
			'priority'    => 160,
		)
	);

	new \Kirki\Field\Repeater(
		[
			'settings' => 'microplant_setting_contact_emails',
			'label'    => esc_html__('Email list', 'microplant'),
			'section'  => 'microplant_section_contact_emails',
			'priority' => 10,
			'row_label' => array(
				'type'  => 'field',
				'value' => esc_html__('Emails', 'microplant'),
				'field' => 'email_title',
			),
			'button_label' => esc_html__('Add another email', 'microplant'),
			'default'  => [
				[
					'email_title'       => 'Comercial',
					'email_address'     => 'comercial@microplant.com.br',
					'email_target'      => '_blank',
				],
				[
					'email_title'       => 'SAC',
					'email_address'     => 'sac@microplant.com.br',
					'email_target'      => '_blank',
				],
			],
			'fields'   => [
				'email_title'   => [
					'type'        => 'text',
					'label'       => esc_html__('Email Title', 'microplant'),
					'default'     => '',
				],
				'email_address'    => [
					'type'        => 'text',
					'label'       => esc_html__('Email Address', 'microplant'),
					'default'     => '',
				],
				'email_target' => [
					'type'        => 'select',
					'label'       => esc_html__('Target', 'microplant'),
					'default'     => '_blank',
					'choices'     => [
						'_blank' => esc_html__('New Window', 'microplant'),
						'_self'  => esc_html__('Same Frame', 'microplant'),
					],
				],
			],
		]
	);
}
