<?php

/**
 * Kirki Customizer - List of Social Networks
 *
 */

new \Kirki\Section(
	'microplant_section_social_networks',
	array(
		'title'       => esc_html__('Social Networks', 'microplant'),
		'description' => esc_html__('Social networks to use around the site.', 'microplant'),
		'priority'    => 160,
	)
);

new \Kirki\Field\URL(
	[
		'settings' => 'microplant_setting_social_youtube',
		'label'    => esc_html__('You Tube', 'microplant'),
		'section'  => 'microplant_section_social_networks',
		'default'  => 'https://www.youtube.com/',
		'priority' => 10,
	]
);

new \Kirki\Field\URL(
	[
		'settings' => 'microplant_setting_social_facebook',
		'label'    => esc_html__('Facebook', 'microplant'),
		'section'  => 'microplant_section_social_networks',
		'default'  => 'https://www.facebook.com/',
		'priority' => 10,
	]
);

new \Kirki\Field\URL(
	[
		'settings' => 'microplant_setting_social_instagram',
		'label'    => esc_html__('Instagram', 'microplant'),
		'section'  => 'microplant_section_social_networks',
		'default'  => 'https://www.instagram.com/',
		'priority' => 10,
	]
);

new \Kirki\Field\URL(
	[
		'settings' => 'microplant_setting_social_whatsapp',
		'label'    => esc_html__('Whatsapp', 'microplant'),
		'section'  => 'microplant_section_social_networks',
		'default'  => 'https://api.whatsapp.com/send?phone=5511988887777&text=Ol%C3%A1',
		'priority' => 10,
	]
);
