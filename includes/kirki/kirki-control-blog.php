<?php

/**
 * Kirki Customizer - Blog
 *
 */

new \Kirki\Section(
	'microplant_section_blog',
	array(
		'title'       => esc_html__('Blog', 'microplant'),
		'description' => esc_html__('Extra options to customize the blog.', 'microplant'),
		'priority'    => 160,
	)
);

// Format the array of categories to a array of options for kirki
function categoriesOptionsFormated($categories)
{
	$formatedArray = array();

	foreach ($categories as $category) {
		$formatedArray[$category->term_id] = $category->name;
	}

	return $formatedArray;
}

new \Kirki\Field\Select(
	array(
		'settings'    => 'microplant_setting_blog_sticky_category',
		'label'       => esc_html__('Sticky Category', 'microplant'),
		'description' => esc_html__("Choose a category to be highlighted on the blog's main page.", 'microplant'),
		'section'     => 'microplant_section_blog',
		'placeholder' => esc_html__('Choose an category', 'microplant'),
		'choices'     => categoriesOptionsFormated(get_categories()),
	)
);
