<?php

/**
 * Kirki Customizer - Footer
 *
 */

new \Kirki\Section(
	'microplant_section_footer',
	array(
		'title'       => esc_html__('Footer', 'microplant'),
		'description' => esc_html__('Extra options to customize the footer.', 'microplant'),
		'priority'    => 160,
	)
);

new \Kirki\Field\Text(
	array(
		'settings'    => 'microplant_setting_footer_newsletter_title',
		'label'       => esc_html__('Newsletter Title', 'microplant'),
		'section'     => 'microplant_section_footer',
		'default'     => esc_html__('Sign up for our newsletter', 'microplant'),
		'priority'    => 10,
	)
);

new \Kirki\Field\Textarea(
	array(
		'settings'    => 'microplant_setting_footer_newsletter_text',
		'label'       => esc_html__('Newsletter Text', 'microplant'),
		'section'     => 'microplant_section_footer',
		'default'     => esc_html__("Subscribe now so you don't miss any important content!", 'microplant'),
	)
);

new \Kirki\Field\Text(
	array(
		'settings'    => 'microplant_setting_footer_newsletter_form',
		'label'       => esc_html__('Newsletter Form Shortcode', 'microplant'),
		'section'     => 'microplant_section_footer',
		'default'     => '[contact-form-7 id="78bb443" title="Contact Form"]',
		'priority'    => 10,
	)
);
