export default class Header {
	constructor() {
		this.body = document.querySelector( 'body' );

		this.wpAdminBar = document.querySelector( '#wpadminbar' );
		this.wpAdminBarHeight = 0;

		this.header = document.querySelector( '#header' );
		this.headerHeight = 0;

		this.navbar = document.querySelector( '#navbar' );
		this.navbarHeight = 0;

		this.navbarCollapse =
			this.navbar.querySelector( '.navbar-collapse' ) ?? undefined;

		this.viewportX = document.documentElement.clientWidth;
		this.viewportY = document.documentElement.clientHeight;

		this.offset = this.wpAdminBarHeight + this.navbar.offsetHeight + 200;

		this.events();
	}

	events() {
		if ( this.navbar ) {
			[ 'resize', 'load' ].forEach( ( event ) => {
				window.addEventListener( event, () => {
					this.handleSizesReCalc();
					this.handleNavbarCollapseSize();
				} );
			} );

			window.addEventListener( 'scroll', () => {
				this.handleLoggedIn();
				this.handleStickyEffect();
			} );

			this.handleNavbarToggle();
		}
	}

	handleSizesReCalc() {
		this.wpAdminBarHeight = this.wpAdminBar
			? this.wpAdminBar.offsetHeight
			: 0;

		this.headerHeight = this.header ? this.header.offsetHeight : 0;
		this.navbarHeight = this.navbar ? this.navbar.offsetHeight : 0;

		this.viewportX = document.documentElement.clientWidth;
		this.viewportY = document.documentElement.clientHeight;
	}

	handleNavbarCollapseSize() {
		if ( this.navbarCollapse ) {
			if ( this.viewportX < 992 ) {
				this.navbarCollapse.style.height = `calc(100vh - ${
					this.navbarHeight + this.wpAdminBarHeight
				}px)`;
				this.navbarCollapse.style.top = `${
					this.navbarHeight + this.wpAdminBarHeight
				}px`;
			} else {
				this.navbarCollapse.style.height = null;
				this.navbarCollapse.style.top = null;
			}
		}
	}

	handleLoggedIn() {
		if ( this.wpAdminBar ) {
			this.navbar.style.top = `${ this.wpAdminBarHeight }px`;
		} else {
			this.navbar.style.top = 0;
		}
	}

	handleStickyEffect() {
		if ( this.viewportX > 992 && window.scrollY > this.offset ) {
			this.navbar.classList.add( 'is-sticky' );
		} else {
			this.navbar.animate(
				[
					{ top: `${ this.navbarHeight * -1 }px` },
					{ top: `${ this.wpAdminBarHeight }px` },
				],
				{
					duration: 400,
					iterations: 1,
				}
			);

			this.navbar.classList.remove( 'is-sticky' );
		}
	}

	handleNavbarToggle() {
		const navbarToggler = this.navbar.querySelector( '.navbar-toggler' );

		if ( navbarToggler ) {
			const icon = navbarToggler.querySelector( '.navbar-toggler-icon' );
			let target = navbarToggler.dataset.target;

			target = document.querySelector( target );

			if ( target ) {
				navbarToggler.addEventListener( 'click', () => {
					this.body.classList.toggle( 'no-scroll-vertical' );

					navbarToggler.classList.toggle( 'show' );
					target.classList.toggle( 'show' );
					icon.classList.toggle( 'close' );
				} );
			}
		}
	}
}
